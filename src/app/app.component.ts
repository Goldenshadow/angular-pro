import { Component } from '@angular/core';
import * as firebase from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  loadedFeature = 'recipe';

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }

  ngOnInit(){
   firebase.initializeApp({
    apiKey: "AIzaSyDcAntR7ki6vG-j61_6yslxNlYJNda8L5g",
    authDomain: "recipespro-e17f1.firebaseapp.com"
   })
  }
}
